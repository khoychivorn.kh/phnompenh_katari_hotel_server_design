package com.hotel_katari_phnompenh.phnompenh_katari_hotel_server.repository.auth;

import com.hotel_katari_phnompenh.phnompenh_katari_hotel_server.model.auth.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("roleRepository")
public interface RoleRepository extends JpaRepository<Role,Integer> {
    Role findByRole(String role);
}
