package com.hotel_katari_phnompenh.phnompenh_katari_hotel_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhnompenhKatariHotelServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhnompenhKatariHotelServerApplication.class, args);
	}

}
