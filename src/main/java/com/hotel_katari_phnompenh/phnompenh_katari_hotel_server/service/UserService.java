package com.hotel_katari_phnompenh.phnompenh_katari_hotel_server.service;

import com.hotel_katari_phnompenh.phnompenh_katari_hotel_server.model.auth.Role;
import com.hotel_katari_phnompenh.phnompenh_katari_hotel_server.model.auth.User;

import java.util.List;

public interface UserService {
    public User findUserByEmail(String email);
    public void saveUser(User user);
    public void createUser(User user);
    public List<User> findAll();
    public List<Role> findAllRoles();
}
